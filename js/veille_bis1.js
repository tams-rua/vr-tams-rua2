var Airtable = require('airtable');
Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: 'key70l2I9iE1rMpX6'
});
var base = Airtable.base('appzwNchEdGvMM1Fx');

base('Veille').select({
    // Selecting the first 3 records in Tableau:
    view: "Tableau"
}).eachPage(function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.

    records.forEach(function(record) {
        console.log('Retrieved', record.get('Synthèse'));
        document.getElementById('addVeille').innerHTML = document.getElementById('addVeille').innerHTML +  record.get('Synthèse');
    });

    // To fetch the next page of records, call `fetchNextPage`.
    // If there are more records, `page` will get called again.
    // If there are no more records, `done` will get called.
    fetchNextPage();

}, function done(err) {
    if (err) { console.error(err); return; }
});

